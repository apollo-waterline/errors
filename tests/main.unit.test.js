describe("Errors Module", () => {
  describe("ServerError class", () => {
    const { ApolloError, ServerError } = require("../index");
    const apolloErrorInstance = new ApolloError("a message", "A_CODE");
    const serverErrorInstance = new ServerError("a message", "A_CODE");

    it("should be an instance of ApolloError class", () => {
      expect(serverErrorInstance).toBeInstanceOf(ApolloError);
    });

    it("should generate the same error as done with ApolloError", () => {
      const t = () => {
        throw serverErrorInstance;
      };
      expect(t).toThrow(ApolloError);
    });

    it("should have the same extensions", () => {
      console.log("errB.originalError: ", serverErrorInstance.originalError);
      expect(serverErrorInstance.extensions).toEqual(
        apolloErrorInstance.extensions
      );
    });
  });
});
