export * from "apollo-server-errors";
import { ApolloError } from "apollo-server-errors";

export class ServerError extends ApolloError {
  constructor(message, code, properties) {
    super(message, code, properties);
  }
}

export class NotFound extends ApolloError {
  constructor(message, properties) {
    super(message, "NOT_FOUND", properties);
  }
}
